#!/usr/bin/perl -w
# Copyright (c) 2013-2015 Péter Radics.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1.  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
# 2.  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY PÉTER RADICS ''AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL PÉTER RADICS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

sub consumeBrace($);
sub consumeBrace($) {
    my ($rest) = @_;
    return undef unless ($rest =~ /^[(][^()]*/g);
    my $tmp = substr($rest, pos($rest) - 1, 3);
    if ($tmp eq "')'" || $tmp eq "'('") {
        return consumeBrace('(' . substr($rest, pos($rest) + 1));
    }
    $rest = substr($rest, pos($rest));
    my $t = substr($rest, 0, 1);
    if ($t eq ')') {
        return substr($rest, 1);
    }
    elsif ($t eq '(') {
        $rest = consumeBrace($rest);
        return undef unless defined($rest);
        return consumeBrace("($rest");
    }
    return undef;
}

sub checkTwoArgs($) {
    my ($rest) = @_;
    return 0 unless ($rest =~ /^\s*[(]/g);
    $rest = substr($rest, pos($rest));
    while ($rest =~ /^[^(),]*/g) {
        if (pos($rest) > 0) {
            my $tmp = substr($rest, pos($rest) - 1, 3);
            if ($tmp eq "')'" || $tmp eq "'('") {
                $rest = substr($rest, pos($rest) + 1);
                next;
            }
        }
        $rest = substr($rest, pos($rest));
        my $t = substr($rest, 0, 1);
        if ($t eq ',') {
            return 1;
        }
        elsif ($t eq ')') {
            return 0;
        }
        elsif ($t eq '(') {
            $rest = consumeBrace($rest);
            return 0 unless defined($rest);
        }
        else {
            return 0;
        }
    }
    return 0;
}

undef $/;
my $origName = 'assert';
my $newName = 'vmassert';
my $origLen = length($origName);
my $file = <>;
my $start = 0;
while ($file =~ /\b$origName\b/g) {
    my $savePos = pos($file);
    if (checkTwoArgs(substr($file, $savePos))) {
        print(substr($file, $start, ($savePos - $start - $origLen)) . $newName);
        $start = $savePos;
    }
    pos($file) = $savePos;
}
print(substr($file, $start));
