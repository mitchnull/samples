#!/usr/bin/perl -w
#

my $threads = {};
my $hidden = {};

sub showCount($) {
    my ($count) = @_;
    if ($count > 1) {
        printf("%d\n", $count);
    }
}

sub list($) {
    my ($t) = @_;
    return 0 if (!defined($threads->{$t}));
    printf("$t\n");
    return 1;
}

sub printOne($) {
    my ($t) = @_;
    return 0 if (!defined($threads->{$t}));
    printf("$t\n");
    $stack = $threads->{$t};
    foreach my $s (@{$stack}) {
        printf("$s\n");
    }
    return 1;
}

sub hide($) {
    my ($t) = @_;
    return 0 if (!defined($threads->{$t}) || defined($hidden->{$t}));
    $hidden->{$t} = 1;
    printf("hiding: $t\n");
    return 1;
}

sub del($) {
    my ($t) = @_;
    return 0 if (!defined($threads->{$t}));
    delete($threads->{$t});
    printf("Deleted: $t\n");
    return 1;
}

sub strMatch($$) {
    my ($t, $match) = @_;
    return (index($t, $match) != -1);
}

sub patternMatch($$) {
    my ($t, $pattern) = @_;
    return ($t =~ /$pattern/);
}

sub strMatchStack($$) {
    my ($t, $match) = @_;
    return 0 if (!defined($threads->{$t}));

    my $stack = $threads->{$t};
    foreach my $s (@{$stack}) {
        return 1 if (index($s, $match) != -1);
    }
    return 0;
}

sub patternMatchStack($$) {
    my ($t, $pattern) = @_;
    return 0 if (!defined($threads->{$t}));

    my $stack = $threads->{$t};
    foreach my $s (@{$stack}) {
        return 1 if ($s =~ /$pattern/);
    }
    return 0;
}

sub fullStackMatch($$) {
    my ($t, $stackToMatch) = @_;
    return 0 if (!defined($threads->{$t}));

    my $stack = $threads->{$t};
    my $ss = scalar(@{$stack});
    return 0 if ($ss != scalar(@{$stackToMatch}));
    for (my $i = 0; $i < $ss; ++$i) {
        return 0 if ($stack->[$i] ne $stackToMatch->[$i]);
    }
    return 1;
}

sub firstThreadStack() {
    foreach my $t (sort(keys %{$threads})) {
        next if (defined($hidden->{$t}));
        return $threads->{$t};
    }
    return [];
}

sub processAll($$) {
    my ($func, $rev) = @_;
    my $count = 0;
    my @ts = $rev
        ? sort({ $b cmp $a } keys %{$threads})
        : sort(keys %{$threads});
    foreach my $t (@ts) {
        next if (defined($hidden->{$t}));
        $count += &{$func}($t);
    }
    showCount($count);
}

sub processNum($$$) {
    my ($func, $count, $rev) = @_;
    my @ts = $rev
        ? sort({ $b cmp $a } keys %{$threads})
        : sort(keys %{$threads});
    foreach my $t (@ts) {
        next if (defined($hidden->{$t}));
        &{$func}($t);
        return if (--$count <= 0);
    }
}

sub processMatch($$$$) {
    my ($func, $matcher, $param, $rev) = @_;
    my $count = 0;
    foreach my $t (sort(keys %{$threads})) {
        next if (defined($hidden->{$t}));
        $count += &{$func}($t) if ($rev ^ &{$matcher}($t, $param));
    }
    showCount($count);
}

sub isnum($) {
    my ($param) = @_;
    return ($param =~ /^[0-9]+\s*/);
}

sub process($$$$) {
    my ($func, $param, $special, $rev) = @_;
    if (!defined($param) || $param eq '') {
        $special
            ? processNum($func, 1, $rev)
            : processAll($func, $rev)
            ;
    }
    elsif (isnum($param)) {
        processNum($func, $param, $rev);
    }
    elsif ($param =~ "^=") {
        processMatch($func, \&fullStackMatch, firstThreadStack(), $rev);
    }
    elsif ($param =~ "^/") {
        $param =~ s/^\///;
        $param =~ s/\/\s*//;
        my $matcher = $special ? \&patternMatchStack : \&patternMatch;
        processMatch($func, $matcher, $param, $rev);
    }
    else {
        if ($param =~ /^"/) {
            $param =~ s/^"//;
            $param =~ s/"$//;
        }
        elsif ($param =~ /^'/) {
            $param =~ s/^'//;
            $param =~ s/'$//;
        }
        my $matcher = $special ? \&strMatchStack : \&strMatch;
        processMatch($func, $matcher, $param, $rev);
    }
}

sub readIn($) {
    my ($fileName) = @_;
    open(my $fh, "<", $fileName) || printf("couldn't open $fileName: $!") &&  return;
    printf("Reading trace from: $fileName...");
    my $tmp = {};
    my $thread;
    my $stack = [];
    while (<$fh>) {
        chomp;
        if (/^[^ \t]/) {
            $tmp->{$thread} = $stack if (defined($thread));
            $thread = $_;
            $stack = [];
            next;
        }
        push(@{$stack}, $_);
    }
    $tmp->{$thread} = $stack if (defined($thread));
    close($fh);
    $threads = $tmp;
    printf(" done.\n");
}

sub writeOut($) {
    my ($fileName) = @_;
    open (my $out, ">", $fileName) || printf("write failed: $!") && return;
    foreach my $t (sort(keys %{$threads})) {
        printf($out "$t\n");
        $stack = $threads->{$t};
        foreach my $s (@{$stack}) {
            printf($out "$s\n");
        }
    }
    close($out);
    printf("written: $fileName\n");
}

sub help() {
    print("q: quit\n" 
        . "w <file>: write current state to <file>\n"
        . "r <file>: read new state from <file>\n"
        . "u: undo to last modification\n"
        . "U: undo to original state\n"
        . "x: expose all hidden threads\n"
        . "----\n"
        . "[!]<cmd> [{<num> | /<pattern>/ | <str>} | =]\n"
        . "  !: reverse\n"
        . "  !: reverse\n"
        . "  uppercase cmd: operate on thread names\n"
        . "  lowercase cmd: operate on stacks\n"
        . "  no param: use first (!: last) element\n"
        . "  <num>: operate on first (!: last) <num> elements\n"
        . "  </pattern/>: operate on elements matching regexp <pattern>\n"
        . "  <str>: operate on elements matching string <str>\n"
        . "  =: match full stack trace of first thread\n"
        . "\n"
        . "  l: list\n"
        . "  p: print\n"
        . "  d: delete\n"
        . "  h: hide\n");
}


# MAIN


my $fileName = shift;
my $tempFileName = "$fileName.tmp";

readIn($fileName);

for(printf("> "); (<>); printf("> ")) {
    chomp;
    my $rev = 0;
    if (/\s*!/) {
        $rev = 1;
        s/\s*!//;
    }
    my ($cmd, $param) = /\s*([^\s\d]+)\s*(.*)$/;
    $param =~ s/\s+$// if (defined($param));
    next if (!defined($cmd));
    if ($cmd =~ /^l/) {
        process(\&list, $param, 1, $rev);
    }
    elsif ($cmd =~/^L/) {
        process(\&list, $param, 0, $rev);
    }
    elsif ($cmd =~ /^p/) {
        process(\&printOne, $param, 1, $rev);
    }
    elsif ($cmd =~ /^P/) {
        process(\&printOne, $param, 0, $rev);
    }
    elsif ($cmd =~ /^d/) {
        writeOut($tempFileName);
        process(\&del, $param, 1, $rev);
    }
    elsif ($cmd =~ /^D/) {
        writeOut($tempFileName);
        process(\&del, $param, 0, $rev);
    }
    elsif (/^h/) {
        process(\&hide, $param, 1, $rev);
    }
    elsif (/^H/) {
        process(\&hide, $param, 0, $rev);
    }
    elsif (/^w/) {
        writeOut($param);
    }
    elsif (/^r/) {
        readIn($param);
    }
    elsif (/^u/) {
        readIn($tempFileName);
    }
    elsif (/^U/) {
        readIn($fileName);
    }
    elsif (/^x/) {
        $hidden = {};
        printf("exposing hidden threads\n");
    }
    elsif (/^q/) {
        last;
    }
    else {
        help();
    }
}
unlink($tempFileName);
exit(0);
