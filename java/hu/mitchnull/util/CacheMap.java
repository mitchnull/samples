package hu.mitchnull.util;

import java.util.Map;

public interface CacheMap<K, V> extends Map<K, V> {

    /**
     * cleanup stale references.
     */
    public void cleanup();
    
    /**
     * Put a new key / value pair into the cache unless it already contains
     * a valid value for the given key.
     * @returns the value associated with the key after the operation (either the
     * original value from the cache or the newly inserted value).
     */
    public V putOrGet(K key, V value);
}
