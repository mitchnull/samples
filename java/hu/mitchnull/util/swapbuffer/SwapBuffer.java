/*
 * !!! WARNING !!!
 * This is a stripped-down version of a working "SwapBuffer" implementation
 * just for demonstration purposes. This code as seen here is not tested,
 * not very well commented and is probably broken in other spectacular ways.
 */

package hu.mitchnull.util.swapbuffer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;

public abstract class SwapBuffer<Elem> {
    
    /**
     * Helper class for Elem logging / writing out to flat-file, etc
     */
    public static interface Dumper<Elem> {
        public void dump(Elem msg);
        public void println(String text);
    }

    /**
     * Implementation of Dumper that does nothging with the Elem
     */
    public static class SilentDumper<Elem> implements Dumper<Elem> {
        @Override
        public void dump(Elem msg) {
        }
        @Override
        public void println(String text) {
        }
    }
    
 
    protected final ReentrantLock writeLock_ = new ReentrantLock();
    protected final ReentrantLock storeLock_ = new ReentrantLock();
    private Set<Elem> main_ = new HashSet<Elem>();
    private Set<Elem> swap_ = new HashSet<Elem>();
    private Set<Elem> failed_ = new HashSet<Elem>();
    private int swapSize_ = 0;
    private int failedSize_ = 0;
    
    private static final int BUFFER_SIZE_MINOR = 200;
    private static final int MAX_NUM_FAILED = 10;
    
    private int bufferSizeMinor_ = BUFFER_SIZE_MINOR;
    
    private boolean isInitialized_ = false;
    private boolean isError_ = false;
    private long lastCommitTime_ = 0;
    private boolean flusherWokenUp_ = false;
    
    protected Logger logger_ = Logger.getLogger(getClass());
    
    private String name_;
    
    public final String getName() {
        return name_;
    }
    
    public class Flusher /* [### SNIP] extends PeriodicJob */ {
        // PeriodicJob handles all the scheduling, it'll call doPeriodicJob() when necessary
        
        public void stop() {} // ### comes from PeriodicJob
        public void start() {} // ### comes from PeriodicJob
        public void wakeup() {} // ### comes from PeriodicJob
        
        public String getName() { return "StorageFlusher[" + name_ + "]"; }
        public void doPeriodicJob() {
            try {
                commit();
            }
            catch (Throwable e) {
            }
        }
    }
    
    private Flusher flusher_;

    /**
     * commitImpl should write out batch to db/file, etc
     */
    protected abstract void commitImpl(Collection<Elem> batch) throws IOException;
    /**
     * additional initialization needed by subclasses, called from tryInit()
     */
    protected abstract void tryInitImpl() throws IOException;
    /**
     * additional teardown needed by subclasses, called from finish()
     */
    protected abstract void finishImpl();
    

    public SwapBuffer(String name) {
        name_ = name;
        flusher_ = new Flusher();
    }
    
    public Flusher getFlusher() {
        return flusher_;
    }
    
    public void tryInit() throws IOException {
        logger_.info(getName() + " starting up");
        try {
            writeLock_.lock();
            try {
                storeLock_.lock();
                bufferSizeMinor_ = 10; // ### DUMMY, get from some prop file or something
                if (isInitialized_) {
                    logger_.info(getName() + " re-initialized");
                    return;
                }
                tryInitImpl();
                isInitialized_ = true;
            }
            finally {
                storeLock_.unlock();
            }
        }
        finally {
            writeLock_.unlock();
        }
        logger_.info(getName() + " initialized");
    }
    
    public void finish() {
        logger_.info(getName() + " shutting down");
        flusher_.stop();
        writeLock_.lock();
        try {
            try {
                flush();
            }
            catch (Throwable e) {
                logger_.warn(getName() + ".shutdown():  flush failed: " + e);
            }
            if (failed_.size() > 0) {
                logger_.info(getName() + ".shutdown():  flushing failed messages: " + failed_.size());
                try {
                    flushFailed(new SilentDumper<Elem>()); // FIXME: dump out to a unique file
                }
                catch (Throwable ig) {
                }
            }
            try {
                finishImpl();
            }
            catch (Throwable e) {
                logger_.warn("unexpected exception in " + getName() + ".finishImpl(): " + e);
            }
            try {
                storeLock_.lock();
                isInitialized_ = false;
            }
            finally {
                storeLock_.unlock();
            }
            
        }
        finally {
            writeLock_.unlock();
        }
        logger_.info(getName() + " shutdown coplete");
    }
    
    private void wakeupFlusher() {
        if (!flusherWokenUp_) {
            flusher_.wakeup();
            flusherWokenUp_ = true;
        }
    }
    
    // storeLock_ must be held
    private void checkBufferSize() {
        int size = swapSize_ + failedSize_ + main_.size();

        // [### SNIP] alarm handling was here
        
        if (size >= bufferSizeMinor_) {
            wakeupFlusher();
        }
    }
    /*
     * Commit recs one-by-one, place failed messages into failed_, unless maxNumFailed consecutive
     * messages are failed, in which case put the last failed messages back to recs.
     * Successfully committed messages and messages placed into failed_ are removed from recs.
     * If any single commit fails, the last exception is thrown when finished.
     */
    private void doCommitOneByOne(Set<Elem> recs, int maxNumFailed, boolean forced, Dumper<Elem> failedDumper) throws IOException {
        int numFailed = 0;
        ArrayList<Elem> tmpRecs = new ArrayList<Elem>(1);
        tmpRecs.add(null);
        ArrayList<Elem> tmpFailed = new ArrayList<Elem>(maxNumFailed);
        Throwable lastException = null;
        for (Iterator<Elem> it = recs.iterator(); it.hasNext(); ) {
            Elem m = it.next();
            tmpRecs.set(0, m);
            try {
                commitImpl(tmpRecs);
                it.remove();
                if (numFailed > 0) {
                    numFailed = 0;
                    failed_.addAll(tmpFailed);
                    tmpFailed.clear();
                }
            }
            catch (Throwable e) {
                it.remove();
                if (failedDumper != null) {
                    try {
                        failedDumper.dump(m);
                    }
                    catch (Throwable ig) {
                    }
                }
                if (!forced) {
                    if (numFailed >= maxNumFailed) {
                        recs.addAll(tmpFailed);
                        throw new IOException(e.getMessage(), e);
                    }
                    lastException = e;
                    tmpFailed.add(m);
                    ++numFailed;
                    try {
                        logger_.debug(getName() + ": failed to commit message: " + e + ": " + m);
                    }
                    catch (Throwable ig) {
                    }
                }
                else {
                    try {
                        logger_.info(getName() + ": failed to commit message: " + e + ": " + m);
                    }
                    catch (Throwable ig) {
                    }
                }
            }
        }
        if (numFailed > 0) {
            failed_.addAll(tmpFailed);
        }
        if (lastException != null) {
            throw new IOException(lastException.getMessage(), lastException);
        }
    }
    
    public void dumpFailed(Dumper<Elem> dumper) {
        writeLock_.lock();
        try {
            for (Elem m : failed_) {
                try {
                    dumper.dump(m);
                }
                catch (Throwable e) {
                    logger_.warn(getName() + ".dumpFailed: ERROR: " + e);
                }
            }
        }
        finally {
            writeLock_.unlock();
        }
    }
    
    public void flushFailed(Dumper<Elem> dumper) throws IOException {
        writeLock_.lock();
        try {
            try {
                storeLock_.lock();
                if (!isInitialized_) {
                    throw new IOException("not initialized");
                }
            }
            finally {
                storeLock_.unlock();
            }
            doCommitOneByOne(failed_, 0, true, dumper);
        }
        finally {
            writeLock_.unlock();
        }
    }
    
    public void flush() throws IOException {
        writeLock_.lock();
        try {
            commit();
            commit(); // commit previous swap, too
        }
        finally {
            writeLock_.unlock();
        }
    }
    
    private void commit() throws IOException {
        boolean idle = true;
        writeLock_.lock();
        try {
            storeLock_.lock();
            try {
                if (!isInitialized_) {
                    return;
                }
                flusherWokenUp_ = false;
                Set<Elem> tmp = main_;
                main_ = swap_;
                swap_ = tmp;
                swapSize_ = swap_.size();
                if (swapSize_ == 0) {
                    return;
                }
            }
            finally {
                storeLock_.unlock();
            }
            idle = false;
            try {
                commitImpl(swap_);
            }
            catch (Throwable e) {
                logger_.warn(getName() + ".commitImpl() FAILED: " + e);
                doCommitOneByOne(swap_, MAX_NUM_FAILED, false, null);
            }
            swap_.clear();
            if (!failed_.isEmpty()) {
                doCommitOneByOne(failed_, 0, false, null);
            }
                    
            if (isError_) {
                logger_.info(getName() + ".commit(): OK");
                // [### SNIP] raise alarm
                isError_ = false;
            }
        }
        catch (Throwable ex) {
            if (!isError_) {
                logger_.error(getName() + ".commit() failed: " + ex);
                // [### SNIP] raise alarm
                isError_ = true;
            }
            throw new IOException("Exception in SmsSwapBuffer[" + name_ + "].commit(): " + ex, ex);
        }
        finally {
            try {
                if (!idle) {
                    storeLock_.lock();
                    try {
                        swapSize_ = swap_.size();
                        failedSize_ = failed_.size();
                        lastCommitTime_ = System.currentTimeMillis();
                        // [### SNIP] checkBufferSize() if buffer size alarm is raised
                    }
                    finally {
                        storeLock_.unlock();
                    }
                }
            }
            finally {
                writeLock_.unlock();
            }
        }
    }
    
    /**
     * Place new Elem into the buffer
     */
    public void store(Elem msg) {
        storeLock_.lock();
        try {
            main_.add(msg);
            checkBufferSize();
        }
        finally {
            storeLock_.unlock();
        }
    }
    
    /**
     * Place new Elems into the buffer
     */
    public void store(Collection<Elem> msgs) {
        storeLock_.lock();
        try {
            main_.addAll(msgs);
            checkBufferSize();
        }
        finally {
            storeLock_.unlock();
        }
    }
    
    /**
     *  needs writeLock_
     */
    protected final void removeLocked(Elem msg) {
        try {
            storeLock_.lock();
            main_.remove(msg);
            swap_.remove(msg);
            failed_.remove(msg);
        }
        finally {
            storeLock_.unlock();
        }
    }
    
    /**
     * needs writeLock_
     */
    protected final void removeAllLocked(Collection<Elem> msgs) {
        try {
            storeLock_.lock();
            main_.removeAll(msgs);
            swap_.removeAll(msgs);
            failed_.removeAll(msgs);
        }
        finally {
            storeLock_.unlock();
        }
    }
    
    /**
     * Remove an Elem from the buffer
     */
    public void remove(Elem msg) {
        try {
            writeLock_.lock();
            removeLocked(msg);
        }
        finally {
            writeLock_.unlock();
        }
    }
    
    /**
     * Remove Elems from the buffer
     */
    public void removeAll(Collection<Elem> msgs) {
        try {
            writeLock_.lock();
            removeAllLocked(msgs);
        }
        finally {
            writeLock_.unlock();
        }
    }
    
    public Date getLastCommitTime() {
        storeLock_.lock();
        try {
            if (lastCommitTime_ == 0) {
                return null;
            }
            return new Date(lastCommitTime_);
        }
        finally {
            storeLock_.unlock();
        }
    }
    
    public String getStatusLine() {
        try {
            storeLock_.lock();
            StringBuilder sb = new StringBuilder();
            sb.append(getName()).append(": ").append(main_.size()).append('/').append(swapSize_);
            if (failedSize_ > 0) {
                sb.append(" FAILED: ").append(failedSize_);
            }
            sb.append(" Last commit: ");
            if (lastCommitTime_ != 0) {
                sb.append(new Date(lastCommitTime_));
            }
            else {
                sb.append("NEVER");
            }
            if (!isInitialized_) {
                sb.append(" (NOT INITIALIZED)");
            }
            return sb.toString();
        }
        finally {
            storeLock_.unlock();
        }
    }
    
    public boolean isInitialized() {
        try {
            storeLock_.lock();
            return isInitialized_;
        }
        finally {
            storeLock_.unlock();
        }
    }
    
}
