package hu.mitchnull.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrentCacheMap<K, V> implements CacheMap<K, V>, Serializable {
    private static final long serialVersionUID = 1L;
    
    private SimpleCacheMap<K, V> cm_ = new SimpleCacheMap<K, V>();
    public final Lock readLock;
    public final Lock writeLock;
    
    /**
     *  Uses ReentrantReadWriteLock
     */
    public ConcurrentCacheMap() {
        this(true);
    }
    
    /**
     * Uses a ReentratLock for both readLock and writeLock if useReadWriteLock is false,
     * or ReentrantReadWriteLock if useReadWriteLock is true.
     */
    public ConcurrentCacheMap(boolean useReadWriteLock) {
        if (useReadWriteLock) {
            ReadWriteLock rwl = new ReentrantReadWriteLock();
            readLock = rwl.readLock();
            writeLock = rwl.writeLock();
        }
        else {
            readLock = writeLock = new ReentrantLock();
        }
    }
    
    /**
     * Uses the supplied locks.
     */
    public ConcurrentCacheMap(Lock readLock, Lock writeLock) {
        this.readLock = readLock;
        this.writeLock = writeLock;
    }

    /** requires write lock */
    public void locked_cleanup() {
		cm_.cleanup();
    }
    
    @Override
    public void cleanup() {
        writeLock.lock();
        try {
            cm_.cleanup();
        }
        finally {
            writeLock.unlock();
        }
    }
    
    /** requires readLock */
    public int locked_size() {
        return cm_.size();
    }
    
    @Override
    public int size() {
        readLock.lock();
        try {
            return cm_.size();
        }
        finally {
            readLock.unlock();
        }
    }

    /** requires readLock */
    public boolean locked_isEmpty() {
        return cm_.isEmpty();
    }
    
    @Override
    public boolean isEmpty() {
        readLock.lock();
        try {
            return cm_.isEmpty();
        }
        finally {
            readLock.unlock();
        }
    }

    /** requires readLock */
    public boolean locked_containsKey(Object key) {
        return cm_.containsKey(key);
    }
    
    @Override
    public boolean containsKey(Object key) {
        readLock.lock();
        try {
            return cm_.containsKey(key);
        }
        finally {
            readLock.unlock();
        }
    }

    /** requires readLock */
    public boolean locked_containsValue(Object value) {
        return cm_.containsValue(value);
    }
    
    @Override
    public boolean containsValue(Object value) {
        readLock.lock();
        try {
            return cm_.containsValue(value);
        }
        finally {
            readLock.unlock();
        }
    }

    /** requires readLock */
    public V locked_get(Object key) {
        return cm_.get(key);
    }
    
    @Override
    public V get(Object key) {
        readLock.lock();
        try {
            return cm_.get(key);
        }
        finally {
            readLock.unlock();
        }
    }

    /** requires writeLock */
    public V locked_put(K key, V value) {
        return cm_.put(key, value);
    }
    
    @Override
    public V put(K key, V value) {
        writeLock.lock();
        try {
            return cm_.put(key, value);
        }
        finally {
            writeLock.unlock();
        }
    }

    /** requires writeLock */
    public V locked_putOrGet(K key, V value) {
        return cm_.putOrGet(key, value);
    }
    
    public V putOrGet(K key, V value) {
        writeLock.lock();
        try {
            return cm_.putOrGet(key, value);
        }
        finally {
            writeLock.unlock();
        }
    }
    
    /** requires writeLock */
    public V locked_remove(Object key) {
        return cm_.remove(key);
    }
    
    @Override
    public V remove(Object key) {
        writeLock.lock();
        try {
            return cm_.remove(key);
        }
        finally {
            writeLock.unlock();
        }
    }

    /** requires writeLock */
    public void locked_putAll(Map<? extends K, ? extends V> m) {
        cm_.putAll(m);
    }
    
    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        writeLock.lock();
        try {
            cm_.putAll(m);
        }
        finally {
            writeLock.unlock();
        }
    }

    /** requires writeLock */
    public void locked_clear() {
        cm_.clear();
    }
    
    @Override
    public void clear() {
        writeLock.lock();
        try {
            cm_.clear();
        }
        finally {
            writeLock.unlock();
        }
    }

    /**
     * Returns a copy of the current keySet. The returned Set is _not_ backed by the underlying CacheMap.
     * The returned Set may contain keys to stale entries.
     */
    public Set<K> currentKeySet() {
        readLock.lock();
        try {
            return new HashSet<K>(cm_.keySet());
        }
        finally {
            readLock.unlock();
        }
    }
    
    /**
     * Requires writeLock if the returned Set will be modified,
     * and readLock if the returned Set will only be read.
     * 
     * In either case the lock must be held until all operations are finished on the returned Set.
     */
    public Set<K> locked_keySet() {
        return cm_.keySet();
    }
    
    @Override
    /** Unsupported, use locked_keySet() or currentKeySet() */
    public Set<K> keySet() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    /** Unsupported */
    public Collection<V> values() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    /** Unsupported */
    public Set<java.util.Map.Entry<K, V>> entrySet() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

}
