package hu.mitchnull.util;

import java.util.AbstractList;
import java.util.Collection;
import java.util.RandomAccess;

public class CircularArray<E> extends AbstractList<E> implements RandomAccess {

    private final int capacity_;
    private int size_ = 0;
    private int front_ = 0;
    private int back_ = -1;
    
    private E[] elems_;
    
    public CircularArray(int capacity) {
        capacity_ = capacity;
        elems_ = CircularArray.<E>allocate(capacity); // some javac bug, should be simple elems_ = allocate(capacity);
    }
    
    public CircularArray(int capacity, Collection<? extends E> initValues) {
        this(capacity);
        addAll(initValues);
    }
    
    public boolean add(E e) {
        if (size_ >= capacity_) {
            throw new ArrayIndexOutOfBoundsException(capacity_);
        }
        modified();
        back_ = mod(back_ + 1);
        elems_[back_] = e;
        ++size_;
        return true;
    }

    public void add(int index, E e) {
        if (size_ >= capacity_ || index < 0 || index > size_) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        modified();
        final int rs = size_ - index;
        if (index < rs) {
            // shift left
            if (index > 0) {
                move(0, -1, index);
            }
            front_ = mod(front_ - 1);
            elems_[internal(index)] = e;
        }
        else {
            // shift right
            if (rs > 0) {
                move(index, index + 1, rs);
            }
            back_ = mod(back_ + 1);
            elems_[internal(index)] = e;
        }
        ++size_;
    }

    public boolean addAll(Collection<? extends E> c) {
        Object[] a = c.toArray();
        final int al = a.length;
        if (al == 0) {
            return false;
        }
        if (size_ + al > capacity_) {
            throw new ArrayIndexOutOfBoundsException(capacity_);
        }
        modified();
        copy(a, size_, al);
        back_ = mod(back_ + al);
        size_ += al;
        return true;
    }

    public boolean addAll(int index, Collection<? extends E> c) {
        Object[] a = c.toArray();
        final int al = a.length;
        if (al == 0) {
            return false;
        }
        if (size_ + al > capacity_) {
            throw new ArrayIndexOutOfBoundsException(capacity_);
        }
        modified();
        final int rs = size_ - index;
        if (index < rs) {
            // shift left
            if (index > 0) {
                move(0, -al, index);
            }
            front_ = mod(front_ - al);
        }
        else {
            // shift right
            if (rs > 0) {
                move(index, index + al, rs);
            }
            back_ = mod(back_ + al);
        }
        copy(a, index, al);
        size_ += al;
        return false;
    }

    public void clear() {
        while (size_ > 0) {
            elems_[front_] = null;
            front_ = mod(front_ + 1);
            --size_;
        }
        front_ = 0;
        back_ = -1;
    }

    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    public E get(int index) {
        checkRange(index);
        return elems_[internal(index)];
    }

    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size_; ++i) {
                if (elems_[internal(i)] == null) {
                    return i;
                }
            }
        }
        else {
            for (int i = 0; i < size_; ++i) {
                if (o.equals(elems_[internal(i)])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = size_ - 1 ; i >= 0; --i) {
                if (elems_[internal(i)] == null) {
                    return i;
                }
            }
        }
        else {
            for (int i = size_ - 1 ; i >= 0; --i) {
                if (o.equals(elems_[internal(i)])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean isEmpty() {
        return size_ == 0;
    }
    
    public boolean remove(Object o) {
        int idx = indexOf(o);
        if (idx < 0) {
            return false;
        }
        removeNoCheck(idx);
        return true;
    }

    public E remove(int index) {
        checkRange(index);
        E res = elems_[internal(index)];
        removeNoCheck(index);
        return res;
    }

    public E set(int index, E e) {
        checkRange(index);
        int i = internal(index);
        E res = elems_[i];
        elems_[i] = e;
        return res;
    }

    public final int size() {
        return size_;
    }


    public Object[] toArray() {
        return toArray(new Object[size_]);
    }

    public <T> T[] toArray(T[] a) {
        if (a.length < size_) {
            a = CircularArray.<T>allocate(size_); // some javac bug, should be simple a = allocate(size_);
        }
        if (front_ + size_ <= capacity_) {
            System.arraycopy(elems_, front_, a, 0, size_);
        }
        else {
            int fs = capacity_ - front_;
            System.arraycopy(elems_, front_, a, 0, fs);
            System.arraycopy(elems_, 0, a, fs, size_ - fs);
        }
        if (a.length > size_) {
            a[size_] = null;
        }
        return a;
    }
    
    /**
     * implemented by AbstractList
     *

    public boolean containsAll(Collection<?> c) {
        return false;
    }

    public Iterator<E> iterator() {
        return null;
    }

    public ListIterator<E> listIterator() {
        return null;
    }

    public ListIterator<E> listIterator(int index) {
        return null;
    }

    public boolean removeAll(Collection<?> c) {
        return false;
    }

    public boolean retainAll(Collection<?> c) {
        return false;
    }

    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }


     */
    
    // ------------------ Impl --------------------- //
    
    private void checkRange(int index) {
        if (index < 0 || index >= size_) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }
    
    private void modified() {
        ++modCount;
    }
    
    @SuppressWarnings("unchecked")
    protected static <T> T[] allocate(int size) {
        return (T[]) new Object[size];
    }
    
    protected int internal(int index) {
        return mod(front_ + index);
    }
    
    protected void move(int fromIndex, int toIndex, int len) {
        int fi = internal(fromIndex);
        int ti = internal(toIndex);
        final int fsf = capacity_ - fi;
        final int fst = capacity_ - ti;
        if (fsf >= len && fst >= len) {
            System.arraycopy(elems_, fi, elems_, ti, len);
            return;
        }
        if (toIndex > fromIndex) {
            // tail first
            for (int i = len - 1; i >= 0; --i) {
                elems_[mod(ti + i)] = elems_[mod(fi + i)];
            }
        }
        else {
            // head first
            for (int i = 0; i < len; ++i) {
                elems_[mod(ti + i)] = elems_[mod(fi + i)];
            }
        }
    }
    
    protected void copy(Object[] src, int index, int len) {
        final int i = internal(index);
        if (i + len <= capacity_) {
            System.arraycopy(src, 0, elems_, i, len);
        }
        else {
            final int fs = capacity_ - i;
            System.arraycopy(src, 0, elems_, i, fs);
            System.arraycopy(src, fs, elems_, 0, len - fs);
        }
    }
    
    protected void removeNoCheck(int index) {
        modified();
        final int rs = (size_ - index) - 1;
        if (index < rs) {
            if (index > 0) {
                move(0, 1, index);
            }
            elems_[front_] = null;
            front_ = mod(front_ + 1);
        }
        else {
            if (rs > 0) {
                move(index + 1, index, rs);
            }
            elems_[back_] = null;
            back_ = mod(back_ - 1);
        }
        --size_;
    }
    
    public String toString() {
        String res = "c:" + capacity_ + " s:" + size_ + " f:" + front_ + " b:" + back_;
        res += "[";
        for (int i = 0 ; i < capacity_; ++i) {
            res += "\t";
            if (i == front_) {
                res += "^";
            }
            if (i == back_) {
                res += "$";
            }
            if (elems_[i] == null) {
                res += "-";
            }
            else {
                res += String.valueOf(elems_[i]);
            }
        }
        res += "]";
        return res;
    }
    
    protected final int mod(int i) {
        i %= capacity_;
        if (i < 0) {
            i += capacity_;
        }
        return i;
    }
    
    protected static final int min(int a, int b) {
        return a <= b ? a : b;
    }
}
