package hu.mitchnull.util;

import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SimpleCacheMap<K, V> implements CacheMap<K, V>, Serializable {
    private static final long serialVersionUID = 1L;

    private static class ValueRef<K, V> extends SoftReference<V> {

        private final K key_;
        
        public ValueRef(K key, V referent, ReferenceQueue<? super V> q) {
            super(referent, q);
            key_ = key;
        }
    }
    
    private final Map<K, ValueRef<K, V>> map_ = new HashMap<K, ValueRef<K, V>>();
    private ReferenceQueue<V> refQueue_ = new ReferenceQueue<V>();

    @SuppressWarnings("unchecked")
    private void cleanupStaleRefs() {
        Reference<?> vr;
        while ((vr = refQueue_.poll()) != null) {
            final ValueRef<K, V> curr = map_.remove(((ValueRef<K, V>)vr).key_);
            if (curr != vr && curr != null && curr.get() != null && !curr.isEnqueued()) {
                // a new entry was inserted already, don't remove it
                map_.put(curr.key_, curr);
            }
        }
    }
    
    public SimpleCacheMap() {
    }
    
    @Override
    public void cleanup() {
        cleanupStaleRefs();
    }

    @Override
    public int size() {
        return map_.size();
    }

    @Override
    public boolean isEmpty() {
        return map_.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map_.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        if (value == null) {
            return false;
        }
        for (ValueRef<K, V> vr : map_.values()) {
            if (value.equals(vr.get())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        ValueRef<K, V> vr = map_.get(key);
        if (vr == null) {
            return null;
        }
        return vr.get();
    }
    
    @Override
    public V putOrGet(K key, V value) {
        cleanupStaleRefs();
        final ValueRef<K, V> newVR =  new ValueRef<K, V>(key, value, refQueue_);
        final ValueRef<K, V> origVR = map_.put(key, newVR);
        if (origVR == null || origVR.get() == null || origVR.isEnqueued()) {
            // new key/value, or the old value is already stale
            return value;
        }
        // already in cache, rollback
        map_.put(key, origVR);
        newVR.clear(); // we don't want this ValueRef to be enqueued
        return origVR.get();
    }

    // get the value from vr then clean it up so that it won't get enqueued
    private V getAndClear(ValueRef<K, V> vr) {
        if (vr == null) {
            return null;
        }
        final V res = vr.get();
        vr.clear(); 
        return res;
    }
    
    private void clear(ValueRef<K, V> vr) {
        if (vr == null) {
            return;
        }
        vr.clear();
    }
    
    @Override
    public V put(K key, V value) {
        cleanupStaleRefs();
        final ValueRef<K, V> vr = map_.put(key, new ValueRef<K, V>(key, value, refQueue_));
        return getAndClear(vr);
    }
    
    @Override
    public V remove(Object key) {
        cleanupStaleRefs();
        final ValueRef<K, V> vr = map_.remove(key);
        return getAndClear(vr);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        cleanupStaleRefs();
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
            final K key = e.getKey();
            clear(map_.put(key, new ValueRef<K, V>(key, e.getValue(), refQueue_)));
        }
    }

    @Override
    public void clear() {
        for (Map.Entry<K, ValueRef<K, V>> entry : map_.entrySet()) {
            clear(entry.getValue());
        }
        map_.clear();
        while (refQueue_.poll() != null) {
            continue;
        }
    }

    @Override
    public Set<K> keySet() {
        return map_.keySet();
    }

    @Override
    public Collection<V> values() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<java.util.Map.Entry<K, V>> entrySet() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

}
