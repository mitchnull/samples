# meanbot.pl
#
# irssi script to change javabot's "what does that even *mean*?" message.
#
# most of the code is stolen from nocaps.pl (JamesOff, Ion)
#

use strict;
use vars qw($VERSION %IRSSI);

use Irssi;

$VERSION = '1.0';
%IRSSI = (
    authors => 'mitchnull',
    name => 'meanbot',
    description => "Replaves javabot's *mean* message",
    license => 'Public Domain',
    changed => '2014-09-29',
);

my $mean_nick = 'javabot';
my $mean_msg = 'what does that even \*mean\*\?';
my $not_so_mean_msg = 'dunno about that';

#main event handler
sub mean_message {
    my ($server, $data, $nick, $address) = @_;

    if ($nick eq $mean_nick && $data =~ $mean_msg) {
        $data =~ s/$mean_msg/$not_so_mean_msg/;
        Irssi::signal_emit('event privmsg', ($server, $data, $nick, $address));
        Irssi::signal_stop();
    }
}

Irssi::signal_add('event privmsg', 'mean_message');
