#include <iostream>
#include <vector>
#include <string>
#include <variant>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <chrono>
#include <thread>

using std::size_t;
using Idx = size_t;
using Number = int; // should be [-999 .. 999]
using Char = char;
using Var = std::variant<std::nullptr_t, Number, Char>;
using Dict = std::unordered_map<std::string, Idx>;

static bool showRunningLine = true;
static int delay = 0;
constexpr const auto npos = std::string::npos;

struct LabelInst {
  std::string label;
  std::string inst;
  Idx lineNo = 0;
};

static std::vector<LabelInst> source;
static std::vector<Var> table;
static Dict tableLabels;
static Dict jumpLabels;

static bool
isComment(const std::string &line) {
  return line.empty() || line.front() == '#';
}

static std::string
readLine(std::istream &in) {
  std::string line;
  std::getline(in, line);
  boost::trim(line);
  boost::to_upper(line);
  return line;
}

static std::string
readUsefulLine(std::istream &in) {
  std::string line;
  while (in) {
    line = readLine(in);
    if (!isComment(line)) {
      break;
    }
    continue;
  }
  return line;
}

static void
error(std::string msg) {
  std::cerr << "\n" << msg << "\n";
  std::exit(1);
}

template <typename Ex>
static void
error(std::string msg, const Ex &e) {
  std::cerr << "\n" << msg << "\n" << e.what() << "\n";
  std::exit(1);
}

template <typename T>
static void
error(const T &msg) {
  std::cerr << "\n"<< msg << "\n";
  std::exit(1);
}

static Var
parseVar(std::string v) {
  if (v.empty()) {
    return {};
  }
  try {
    return {boost::lexical_cast<Number>(v)};
  }
  catch (...) {
  }
  return {v[0]};
}

static std::string
toString(const Var &v) {
  if (std::holds_alternative<Number>(v)) {
    return std::to_string(std::get<Number>(v));
  }
  if (std::holds_alternative<Char>(v)) {
    return {std::get<Char>(v)};
  }
  return {};
}

static std::string
toString(const LabelInst pl) {
    if (!pl.label.empty()) {
      if (isComment(pl.inst)) {
        return (boost::format("\t%1%:\t%2%") % pl.label %  pl.inst).str();
      }
      else {
        return (boost::format("%1%\t%2%:\t%3%") % pl.lineNo % pl.label %  pl.inst).str();
      }

    }
    else if (isComment(pl.inst)) {
      return (boost::format("\t\t%1%") % pl.inst).str();
    }
    else {
      return (boost::format("%1%\t\t%2%") % pl.lineNo %  pl.inst).str();
    }
}

static void
checkNil(Var v) {
  if (std::holds_alternative<nullptr_t>(v)) {
    error("nil value");
  }
}

static Idx
getJmpLoc(std::vector<std::string> cp) {
  if (cp.size() < 2) {
    error("missing label");
  }
  Idx ip = jumpLabels[cp[1]];
  if (ip <= 0) {
    error("unknown label");
  }
  return ip;
}

static Idx
getTableIdx0(std::string op) {
  Idx res;
  try {
    res = boost::lexical_cast<Number>(op);
    if (res >= tableLabels.size()) {
      error("table index out of range");
    }
  }
  catch (...) {
    auto it = tableLabels.find(op);
    if (it == tableLabels.end()) {
      error("unknown table label");
    }
    res = it->second;
  }
  return res;
}

static Idx
getTableIdx(std::vector<std::string> cp) {
  if (cp.size() < 2) {
    error("missing operand");
  }
  Idx res;
  std::string op = cp[1];
  if (op.front() == '[') {
    if (op.back() != ']') {
      error("syntax error, unclosed []");
    }
    std::string ptr = op.substr(1, op.size() - 2);
    Idx ptrIdx = getTableIdx0(ptr);
    Var deref = table[ptrIdx];
    if (!std::holds_alternative<Number>(deref)) {
      error("not a number");
    }
    Number nbr = std::get<Number>(deref);
    if (nbr < 0) {
      error("table index out of range");
    }
    res = nbr;
    if (res >= table.size()) {
      error("table index out of range");
    }
  }
  else {
    res = getTableIdx0(op);
  }
  return res;
}

int
main() {
  std::istream &in = std::cin;
  Idx ip = 0;
  size_t sizeCount = 0;
  size_t instCount = 0;

  std::string line = readUsefulLine(in);
  if (line.empty()) {
    error("invalid input, need table size: Width x Height\n");
  }
  std::vector<std::string> wh;
  boost::split(wh, line, boost::is_any_of(" \tX"), boost::token_compress_on);
  if (wh.size() != 2) {
    error("invalid input, need table size: Width x Height\n");
  }
  size_t tw;
  size_t th;
  try {
    tw = boost::numeric_cast<size_t>(boost::lexical_cast<int>(wh[0]));
    th = boost::numeric_cast<size_t>(boost::lexical_cast<int>(wh[1]));
  }
  catch (std::bad_cast &e) {
    error("invalid input, need table size: Width x Height", e);
  }
  table.reserve(tw * th);
  for (size_t i = 0; i < th; ++i) {
    line = readUsefulLine(in);
    std::vector<std::string> tr;
    boost::split(tr, line, boost::is_any_of(","));
    if (tr.size() != tw) {
      error(boost::format("invalid input, need %1% column(s) in table row %2%.") % tw % (i + 1));
    }
    for (auto &td : tr) {
      boost::trim(td);
      std::vector<std::string> lv;
      boost::split(lv, td, boost::is_any_of(" \t:"), boost::token_compress_on);
      if (lv.size() == 2) {
        auto label = lv[0];
        td = lv[1];
        tableLabels[label] = table.size();
      }
      table.push_back(parseVar(td));
    }
  }
  while (in) {
    line = readLine(in);
    if (line == ".") {
      break;
    }
    std::vector<std::string> li(2);
    boost::split(li, line, boost::is_any_of(":"));
    std::string label;
    std::string inst = line;
    if (li.size() == 2) {
      label = li[0];
      boost::trim(label);
      inst = li[1];
      boost::trim(inst);
    }
    if (!label.empty()) {
      jumpLabels[label] = source.size();
    }
    if (isComment(inst)) {
      source.push_back({label, inst});
    }
    else {
      source.push_back({label, inst, ++sizeCount});
    }
  }

  // end of program source
  std::cout << "\nSOURCE:\n";
  int ln = 0;
  for (auto &pl : source) {
    std::cout << toString(pl) << "\n";
  }
  std::cout << "\nJUMP LABELS:\n";
  for (const auto &ll : jumpLabels) {
    std::cout << ll.first << " -> " << ll.second + 1 << "\n";
  }
  std::cout << "\nTABLE LABELS:\n";
  for (const auto &li : tableLabels) {
    std::cout << li.first << " -> " << li.second << " = " << toString(table[li.second]) << "\n";
  }

  std::cout << "\n";
  Var v;
  while (ip < source.size()) {
    auto pl = source[ip];
    std::string inst = pl.inst;
    if (isComment(inst)) {
      ++ip;
      continue;
    }
    ++instCount;
    // laaaame :)
    if (showRunningLine) {
      std::cerr << "\r                                                                                                       \r";
      std::cerr << "\r" << toString(pl) << " \t[" << toString(v) << "] (" << instCount << ") ";
      std::flush(std::cerr);
      if (delay > 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(delay));
      }
    }
    std::vector<std::string> cp;
    boost::split(cp, inst, boost::is_any_of(" \t"), boost::token_compress_on);
    auto cmd = cp[0];
    if (cmd == "IN") {
      std::string word;
      in >> word;
      if (!in) {
        break;
      }
      v = parseVar(word);
    }
    else if (cmd == "OUT") {
      checkNil(v);
      std::cout << toString(v) << "\n";
      v = {};
    }
    else if (cmd == "JMP") {
      ip = getJmpLoc(cp);
      continue;
    }
    else if (cmd == "JB") {
      checkNil(v);
      if (std::holds_alternative<Number>(v) && std::get<Number>(v) < 0) {
        ip = getJmpLoc(cp);
        continue;
      }
    }
    else if (cmd == "JZ") {
      checkNil(v);
      if (std::holds_alternative<Number>(v) && std::get<Number>(v) == 0) {
        ip = getJmpLoc(cp);
        continue;
      }
    }
    else if (cmd == "ST") {
      checkNil(v);
      Idx ti = getTableIdx(cp);
      table[ti] = v;
    }
    else if (cmd == "LD") {
      Idx ti = getTableIdx(cp);
      v = table[ti];
      checkNil(v);
    }
    else if (cmd == "SUB") {
      checkNil(v);
      Idx ti = getTableIdx(cp);
      Var tv = table[ti];
      try {
        if (std::holds_alternative<Number>(v)) {
          v = std::get<Number>(v) - std::get<Number>(tv);
        }
        else { // Char
          v = static_cast<Number>(std::get<Char>(v)) - static_cast<Number>(std::get<Char>(tv)); 
        }
      }
      catch (...) {
        error("invalid operands");
      }
    }
    else if (cmd == "ADD") {
      checkNil(v);
      Idx ti = getTableIdx(cp);
      Var tv = table[ti];
      try {
        v = std::get<Number>(v) + std::get<Number>(tv);
      }
      catch (...) {
        error("invalid operands");
      }
    }
    else if (cmd == "INC") {
      Idx ti = getTableIdx(cp);
      Var tv = table[ti];
      try {
        v = std::get<Number>(tv) + 1;
      }
      catch (...) {
        error("invalid operand");
      }
      table[ti] = v;
    }
    else if (cmd == "DEC") {
      Idx ti = getTableIdx(cp);
      Var tv = table[ti];
      try {
        v = std::get<Number>(tv) - 1;
      }
      catch (...) {
        error("invalid operand");
      }
      table[ti] = v;
    }
    else {
      error("unknown command");
    }
    ++ip;
  }
  std::cerr << "\nEND: " << instCount << std::endl;
}
